import sys
import argparse
import requests
from tabulate import tabulate

###
#Payload display function
###

def display_get(call, h1, h2, h3):  
    r = requests.get(call)   
    headers = [h1, h2, h3]
    table = []

    for obj in r.json():
        table.append([obj[h1], obj[h2], obj[h3]])

    print(tabulate(table, headers=headers, tablefmt='rst'))

    if r.status_code == 404:
        print("Something went wrong.")
        sys.exit(1)

###
#CLI arguments
###

parser = argparse.ArgumentParser(description="A CLI tool for Gitlab")

group = parser.add_mutually_exclusive_group()
group.add_argument("-pu", "--public", action="store_true", help="set project <id> to public")
group.add_argument("-pr", "--private", action="store_true", help="set project <id> to private")

parser.add_argument("-u", "--user", type=str, help="name of the Gitlab user")
parser.add_argument("-i", "--id", type=int, help="project ID (a number)")
parser.add_argument("-p", "--name", type=str, help="name of project")
parser.add_argument("-l", "--list", type=str, help="list projects for <user>")
parser.add_argument("-n", "--new", action="store_true", help="create project <name> for <user>")
parser.add_argument("-d", "--description", type=str, help="show description for project <id>")
parser.add_argument("-ch", "--describe", type=str, help="change description for project <id>")
parser.add_argument("-rm", "--delete", action="store_true", help="delete project <id>")
parser.add_argument("-t", "--token", type=str, help="access token")
parser.add_argument("-s", "--search", type=str, help="search projects by name")

args = parser.parse_args()


###
#CLI conditions
###

# Description for project
if args.description:
    r = requests.get("https://gitlab.com/api/v4/projects/{args.description}")
    
    if r.status_code >= 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")
        sys.exit(1)        
    

# List projects for user
elif args.list:
    display_get(f'https://gitlab.com/api/v4/users/{args.list}/projects', "id", "name", "description")

    
# Search function
elif args.search:
    r = requests.get(f"https://gitlab.com/api/v4/projects/?search={args.search}")
    if r.status_code >= 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")
           

 # Make project public
elif args.public and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/{args.id}/?visibility=public",
        headers={'PRIVATE-TOKEN': f'{args.token}'},
    )

    if r.status_code >= 200:
        print("Done.")
        print(r.headers['Content-Type'])
    else:
        print(f"Error {r.status_code}. Is your token valid?")
    
    
# Make project private
elif args.private and args.id and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/{args.id}?visibility=private",
        headers={f'PRIVATE-TOKEN': f'{args.token}'},
    )

    if r.status_code >= 200:
        print("Done.")
        print(r.headers['Content-Type'])
    else:
        print(f"Error {r.status_code}. Is your token valid?")

# Create project 
elif args.new and args.name and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/?name={args.name}",
        headers={f'PRIVATE-TOKEN': '{args.token}'},
    )

    if r.status_code >= 200:
        print("Project created.")
    else:
        print(f"Error {r.status_code}. Is your token valid?")

# Change project description
elif args.describe and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/{args.describe}",
        headers={'PRIVATE-TOKEN': f'{args.token}'},
    )

    if r.status_code >= 200:
        print("Done.")
        print(r.headers)['Content-Type']
    else:
        print(f"Error {r.status_code}. Did you specify a token?")

# Delete project
elif args.delete and args.token:
    delete_confirmation = input("Are you sure you want to delete? (Y/n : ")

    if delete_confirmation == 'Y':
        r = requests.delete(
            f"https://gitlab.com/api/v4/projects/{args.id}",
        headers={'PRIVATE-TOKEN': f'{args.token}'},
        )
    
    else:
        print("Deletion aborted.")
    
    if r.status_code >= 200:
        print(f"Project {args.id} deleted.")
    else:
        print(f"Error {r.status_code}. Did you specify a token?")