###
# learning tabulate
###

import argparse
import requests
from tabulate import tabulate
import sys

data = [
    ["id", 25682850, "woooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooow"],
    ["description", "My first steps in Python", "yeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeehhhhhhhhhhhhhhhhhhhhhha"],
    ["name", "Python-noob-stuff", "bbbbbbbbbbbboooooooooooooooooooooooooooooooooooooooooooooooooooooooooh"],
    ["name_with_namespace", "philomelius / Python-noob-stuff in a boat with too many sharks around us but hey, that's life I guess. What do you know"],
    ["path","python-noob-stuff", "hhhhhhhhhhheeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeh"]
   ]

elif args.list:
    r = requests.get(f"https://gitlab.com/api/v4/users/{args.list}/projects")

    headers = ["id", "name", "description"]
    table = []

    for obj in r.json():
        table.append([obj['id'], obj['name'], obj['description']])
    
    print(tabulate(table, headers=headers, tablefmt='rst'))
        
    if r.status_code == 404:
        print("Something went wrong.")
        sys.exit(1)