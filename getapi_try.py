import argparse
import requests
from tabulate import tabulate

###
# Functions
###

def api_call(method, choice):
    print("Contacting server...")
    print("Fetching request {}: ".format(choice))
    r = requests.method('https://gitlab.com/api/v4/projects/{}'.format(choice))

    #r = method('https://gitlab.com/api/v4/projects/', params=[choice])

    if r.status_code == 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")
    
    print(r.content)


def push_data(choice):
    print("Contacting server...")
    print("Pushing data {}: ".format(choice))
    r = requests.put("https://gitlab.com/api/v4/projects/{}".format(choice))

    if r.status_code == 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")
    
    print(r.content)
    #display(r.content)
    #r.headers to force headers
    #r.headers['Content-Type'] to display a specific value and key ---> after changing value



def display(content):
    rows = []
    headers = [" ", " ", " "]

    for key, value in r.items():
        rows.append([key, value])
    print(tabulate(rows, headers=headers))
    

###
#CLI arguments
###

parser = argparse.ArgumentParser(description="CLI tool for Gitlab made by Nicolas Hommais")

group = parser.add_mutually_exclusive_group()
group.add_argument("-pu", "--public", action="store_true", help="set project <id> to public")
group.add_argument("-pr", "--private", action="store_true", help="set project <id> to private")

parser.add_argument("-u", "--user", type=str, help="name of the Gitlab user")
parser.add_argument("-p", "--password", type=str, help="password for the user")
parser.add_argument("-i", "--id", type=int, help="project ID (a number)")
parser.add_argument("-l", "--list", action="store_true", help="list project <id>")
parser.add_argument("-n", "--new", type=str, help="create project <name> for <user>")
parser.add_argument("-d", "--description", type=str, help="show description for project <id>")
parser.add_argument("-rm", "--delete", action="store_true", help="delete project <id>")
parser.add_argument("-t", "--token", type=str, help="access token")
parser.add_argument("-s", "--search", type=str, help="search projects by name")

args = parser.parse_args()

###
#CLI conditions
###

    # GET

if args.description:
    api_call(requests.get, args.description)

elif args.list:
    api_call(get, args.list, "https://gitlab.com/api/v4/projects/?")

elif args.search:
    api_call(get, args.list, "https://gitlab.com/api/v4/projects/?")

    # PUT

elif args.public:
    push_data(put, args.list, "https://gitlab.com/api/v4/projects/?")

elif args.private:
    push_data(put, args.list, "https://gitlab.com/api/v4/projects/?")

elif args.delete:
    push_data(delete, args.list, "https://gitlab.com/api/v4/projects/?")

elif args.new:
    push_data(args.list, "https://gitlab.com/api/v4/projects/?")

elif args.describe:
    push_data(put, args.list, "https://gitlab.com/api/v4/projects/?")









