import argparse
import requests
from tabulate import tabulate


def display(content):
    payload = []
    
    for key, value in content:
        payload.append(key, value)
        print(tabulate(payload, headers='firstrow', tablefmt='fancy_grid'))


###
#CLI arguments
###

parser = argparse.ArgumentParser(description="A CLI tool for Gitlab")

group = parser.add_mutually_exclusive_group()
group.add_argument("-pu", "--public", action="store_true", help="set project <id> to public")
group.add_argument("-pr", "--private", action="store_true", help="set project <id> to private")

parser.add_argument("-u", "--user", type=str, help="name of the Gitlab user")
parser.add_argument("-i", "--id", type=int, help="project ID (a number)")
parser.add_argument("-p", "--name", type=str, help="name of project")
parser.add_argument("-l", "--list", type=str, help="list projects for <user>")
parser.add_argument("-n", "--new", type=str, help="create project <name> for <user>")
parser.add_argument("-d", "--description", type=str, help="show description for project <id>")
parser.add_argument("-ch", "--describe", type=str, help="change description for project <id>")
parser.add_argument("-rm", "--delete", action="store_true", help="delete project <id>")
parser.add_argument("-t", "--token", type=str, help="access token")
parser.add_argument("-s", "--search", type=str, help="search projects by name")

args = parser.parse_args()

###
#CLI conditions
###


# Description for project
if args.description:
    print("Contacting server...")
    print("Getting description for project # {}: ".format(args.description))
    r = requests.get("https://gitlab.com/api/v4/projects/{}".format(args.description))
    
    if r.status_code == 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")

    # Duplicating display function code to see if it works better
    payload = []
    for key, value in r:
        payload.append(key, value)
        print(tabulate(payload, headers='firstrow', tablefmt='fancy_grid'))

    #print(r.content)
    #display(r)

# List projects for user
elif args.list:
    print("Contacting server...")
    print("Listing project for user {}: ".format(args.list))
    r = requests.get("https://gitlab.com/api/v4/users/{}/projects".format(args.list))
    
    if r.status_code == 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")

    print(r.content)
    #display(r.content)

# Search function
elif args.search:
    print("Contacting server...")
    print("Searching projects with '{}': ".format(args.search))
    r = requests.get("https://gitlab.com/api/v4/projects/?search={}".format(args.search))
    if r.status_code == 200:
        print("Here's what I got:")
    elif r.status_code == 404:
        print("Something went wrong.")

    print(r.content)
    #display(r.content)

 # Make project public
elif args.public and args.token:
    print("Contacting server...")
    print("Setting project {} to public: ".format(args.id))
    r = requests.put(
        "https://gitlab.com/api/v4/projects/{}/?visibility=public".format(args.token, args.id),
        headers={"PRIVATE-TOKEN: {}"},
    )

    if r.status_code == 200:
        print("Done.")
        print(r.headers['Content-Type'])
    else:
        print("Something went wrong. Did you specify a token?")
    
    print(r.content)
    #display(r.content)

# Make project private
elif args.private and args.token:
    print("Contacting server...")
    print("Setting project {} to private...".format(args.id))
    r = requests.put(
        "https://gitlab.com/api/v4/projects/{}/?visibility=private".format(args.token, args.id),
        headers={"PRIVATE-TOKEN: {}"},
    )

    if r.status_code == 200:
        print("Done.")
        print(r.headers['Content-Type'])
    else:
        print("Something went wrong. Did you specify a token?")


# Create project 
elif args.new and args.name and args.token:
    print("Contacting server...")
    print("Creating project for user {}: ".format(args.user))
    r = requests.put(
        "https://gitlab.com/api/v4/projects/{}?name={}".format(args.token, args.user, args.name),
        headers={"PRIVATE-TOKEN: {}"},
    )

    if r.status_code == 200:
        print("Project created.")
    else:
        print("Something went wrong. Did you specify a token?")

# Change project description
elif args.describe and args.token:
    print("Contacting server...")
    print("Changing description for project {}: ".format(args.id))
    r = requests.put(
        "https://gitlab.com/api/v4/projects/{}".format(args.token, args.describe),
        headers={"PRIVATE-TOKEN: {}"},
    )

    if r.status_code == 200:
        print("Done.")
        print(r.headers)['Content-Type']
    else:
        print("Something went wrong. Did you specify a token?")

# Delete project
elif args.delete and args.token:
    print("Contacting server...")
    print("Deleting project {}: ".format(args.id))

    delete_confirmation = input("Are you sure you want to delete? (Y/n : ")

    if delete_confirmation == 'Y':
        r = requests.delete(
            "https://gitlab.com/api/v4/projects/{}".format(args.token, args.id),
            headers={"PRIVATE-TOKEN: {}"},
        )
    
    else:
        print("Deletion aborted.")
    
    if r.status_code == 200:
        print(f"Project {args.id} deleted.")
    else:
        print("Something went wrong. Did you specify a token?")

