import argparse
import requests
from tabulate import tabulate

#CLI arguments

parser = argparse.ArgumentParser(description="A CLI tool for Gitlab made by Nicolas Hommais")

group = parser.add_mutually_exclusive_group()
group.add_argument("-pu", "--public", action="store_true", help="set project <id> to public")
group.add_argument("-pr", "--private", action="store_true", help="set project <id> to private")

parser.add_argument("-u", "--user", type=str, help="name of the Gitlab user")
parser.add_argument("-p", "--password", type=str, help="password for the user")
parser.add_argument("-i", "--id", type=int, help="project ID (a number)")
parser.add_argument("-l", "--list", action="store_true", help="list project <id>")
parser.add_argument("-n", "--new", type=str, help="create project <name> for <user>")
parser.add_argument("-d", "--description", action="store_true", help="show description for project <id>")
parser.add_argument("-rm", "--delete", action="store_true", help="delete project <id>")
parser.add_argument("-t", "--token", type=str, help="access token")
args = parser.parse_args()

#CLI conditions

if args.id:
    print(f"Listing files for project #{args.id}: ")
    r = requests.get(f" https://gitlab.com/api/v4/projects/{args.id}")
    print(r.json())
    




# def pull_api():
#     print("Listing project for {args.id}: ")
#     r = requests.get(f" https://gitlab.com/api/v4/projects/{args.id}"))
#     print(r.json())




# if args.verbose >= 2:
#     print("The square of {} equals {}".format(args.square, answer))

# elif args.verbose == 1:
#     print("{}^2 = {}".format(args.square, answer))

# else:
#     print(answer)

