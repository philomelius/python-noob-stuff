##################
# Github API CLT #
##################

# Libraries
import requests
from tabulate import tabulate
import argparse
import json

pull_output = []


# Main function
def main():
    q = input("Do you want to push (P) or pull (L) to the API ? ")
    if q == "L" or q == "l":
        pull_api()
        tabulate_output()
    else:
        print("Wrong choice, try again.")
        menu()


# Pull function
def pull_api():
    # try:
    print("Test URL: https://jsonplaceholder.typicode.com/todos/1")
    r = requests.get(input("Insert URL : "))
    print(f"Console output {r.json()}")
    # except:
    # print("Something is wrong with the URL. Please retry.")


# Tabulate function
def tabulate_output():
    r = requests.get("https://jsonplaceholder.typicode.com/todos/1")
    pull_output.append(r.json)
    print(tabulate(pull_output))

    
# Push function
def push_api():
    print("I'm not coded yet")


# Parser function
def api_parser():
    parser = argparse.ArgumentParser(description='A CLI tool for Gitlab API.')
    parser.add_argument('--user', metavar='N', type=str, nargs='+',
                        help='the name of the Gitlab user')
    parser.add_argument('--list', dest='accumulate', action='store_const',
                        const=sum, default=max,
                        help='Project list for a user')

#Args:  user_id, projects : GET /users/:user_id/projects
#       


def pull_api():
    print("Listing project for {args.id}: https://jsonplaceholder.typicode.com/todos/1")
    r = requests.get(input("Insert URL : "))
    print(f"Console output {r.json()}")
    



if __name__ == "__main__":
    main()
