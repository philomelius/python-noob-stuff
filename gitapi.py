import sys
import argparse
import requests
from tabulate import tabulate

###
#Payload display function
###

def display_get(call, h1, h2, h3):  
    r = requests.get(call)   
    headers = [h1, h2, h3]
    table = []

    for obj in r.json():
        table.append([obj[h1], obj[h2], obj[h3]])

    print(tabulate(table, headers=headers, tablefmt='rst'))

    if r.status_code == 404:
        print("Something went wrong.")
        sys.exit(1)

###
#CLI arguments
###

parser = argparse.ArgumentParser(description="A CLI tool for Gitlab")

group = parser.add_mutually_exclusive_group()
group.add_argument("-pu", "--public", action="store_true", help="set project <id> to public")
group.add_argument("-pr", "--private", action="store_true", help="set project <id> to private")

parser.add_argument("-u", "--user", type=str, help="name of the Gitlab user")
parser.add_argument("-i", "--id", type=str, help="project ID (a number)")
parser.add_argument("-inf", "--info", type=str, help="project ID (a number)")
parser.add_argument("-p", "--name", type=str, help="name of project")
parser.add_argument("-l", "--list", type=str, help="list projects for <user>")
parser.add_argument("-n", "--new", action="store_true", help="create project <name> for <user>")
parser.add_argument("-d", "--describe", type=str, help="change description for project <id>")
parser.add_argument("-rm", "--delete", action="store_true", help="delete project <id>")
parser.add_argument("-t", "--token", type=str, help="access token")

args = parser.parse_args()

###
#CLI conditions
###


# List projects for user
if args.list:
    display_get(f'https://gitlab.com/api/v4/users/{args.list}/projects', "id", "name", "description")


# List project info
elif args.info:
    display_get(f"https://gitlab.com/api/v4/projects/{args.info}", "id", "name", "description")


 # Make project public
elif args.public and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/{args.id}/?visibility=public",
        headers={'PRIVATE-TOKEN': f'{args.token}'},
    )

    if r.status_code >= 200:
        print(f"Project {args.id} set to public.")
        display_get(f"https://gitlab.com/api/v4/projects/{args.id}", "id", "name", "visibility")

    else:
        print(f"Error {r.status_code}. Is your token valid?")
    

# Make project private
elif args.private and args.id and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/{args.id}?visibility=private",
        headers={f'PRIVATE-TOKEN': f'{args.token}'},
    )

    if r.status_code >= 200:
        print(f"Project {args.id} set to private.")
        display_get(f"https://gitlab.com/api/v4/projects/{args.id}", "id", "name", "visibility")

    else:
        print(f"Error {r.status_code}. Is your token valid?")


# Create project 
elif args.new and args.name and args.token:
    r = requests.post(
        f"https://gitlab.com/api/v4/projects/?name={args.name}",
        headers={f'PRIVATE-TOKEN': '{args.token}'},
    )

    if r.status_code >= 200:
        print(f"Project {args.name} created.")
        display_get(f"https://gitlab.com/api/v4/projects/{args.name}", "id", "name", "description")

    else:
        print(f"Error {r.status_code}. Is your token valid?")


# Change project description (private)
elif args.describe and args.id and args.token:
    r = requests.put(
        f"https://gitlab.com/api/v4/projects/{args.id}?{args.describe}",
        headers={'PRIVATE-TOKEN': f'{args.token}'},
    )

    if r.status_code >= 200:
        print("Done.")
        display_get(f"https://gitlab.com/api/v4/projects/{args.id}", "id", "name", "visibility")

    else:
        print(f"Error {r.status_code}. Did you specify a token?")


# Change project description (public)
elif args.describe and args.id:
    r = requests.put(f"https://gitlab.com/api/v4/projects/{args.id}?{args.describe}")

    if r.status_code >= 200:
        print("Done.")
        display_get(f"https://gitlab.com/api/v4/projects/{args.id}", "id", "name", "description")

    else:
        print(f"Error {r.status_code}.")


# Delete project
elif args.delete and args.token:
    delete_confirmation = input("Are you sure you want to delete? (Y/n : ")

    if delete_confirmation == 'Y':
        r = requests.delete(
            f"https://gitlab.com/api/v4/projects/{args.id}",
        headers={'PRIVATE-TOKEN': f'{args.token}'},
        )
    
    else:
        print("Deletion aborted.")
    
    if r.status_code >= 200:
        print(f"Project {args.id} deleted.")
    else:
        print(f"Error {r.status_code}. Did you specify a token?")